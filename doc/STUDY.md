1.**BUT**

Le but de ce document est de comparer les deux frameworks Srping, Vertx afin de choisir la plus adaptée au développement de l'application OrthoPro. Notre choix sera en fonction des critères suivants:
- La richesse des fonctionnalités par rapport à l'utilisation d'OrthoPro.
- La maturité
- Facilité d'intégration et de maintenance évolutive.

**2. METHODOLOGIE**

Nous créons pour chaque framework un prototype : avec lectures en base, renvoi de données au format JSON.
Nous étudions ainsi la façon dont chaque technologie est intégrée dans notre application et de vérifions qu'elle répond à notre besoin.

**3. INTEGRATION DU FRAMEWORK SPRING: **

**3.1 Maturité**: 

Spring est supporté par les grands serveur web comme Weblogic (https://docs.oracle.com/cd/E12839_01/web.1111/e14453/appdev.htm#SPRNG298)  et WebSphere (https://www.ibm.com/support/knowledgecenter/en/SSAW57_8.5.5/com.ibm.websphere.nd.doc/ae/cspr_intro.html).
On peut donc supposer que cette technologie soit bien connue et a fait ses preuves.

** 3.2 Fonctionnalité offertes:**

Spring est très riche en fonctionnalité. Nous nous intéressons dans le cadre de ce prototype, à sa capacité à interfacer avec la base de données et entre le client Web et le serveur Web.

Spring permet au développeur de se débarasser de la gestion de d'ouverture/fermeture de connexion, de transaction (dois je faire un rollback en cas d'exception ou en cas de lancent de test via Junit). Puis, le mapping entre un controller et une requête est simple: juste une annotation. 
Enfin, à l'aide spring.framework.bind.annotation, le changement d'un format de communication (Xml, Jason ...) se fait juste par un changement dans l'annotation du controller comme ceci:

` @GetMapping(path = "grammars", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Grammar> afficher( /*ModelMap pModel*/) {`   
 

**3.3 Comment l'intégrer ? **

**3.3.1  Déclarer les dépendances (Feng_Nguyen\prototype\Ortho_Spring\Ortho_Spring\pom.xml)**

Nous utilisons la version 5:

`<spring.version>5.0.0.RELEASE</spring.version>`


```
<dependency>
			<groupId>org.springframework</groupId>
			<artifactId>spring-webmvc</artifactId>
			<version>${spring.version}</version>
		</dependency>

		<dependency>
			<groupId>org.springframework</groupId>
			<artifactId>spring-orm</artifactId>
			<version>${spring.version}</version>
		</dependency>  	
<dependency>
  		<groupId>org.jboss.logging</groupId>
  		<artifactId>jboss-logging</artifactId>
  		<version>3.2.0.Final</version>
  	</dependency>

  	<dependency>
  		<groupId>com.fasterxml.jackson.core</groupId>
  		<artifactId>jackson-databind</artifactId>
  		<version>2.9.0</version>
  	</dependency>
  </dependencies>
```
    
***BUG**: pour utiliser Spring, il faut intégrer plusieurs packages séparés . Certaines versions sont incompatibles entre elles. C'était un gros point noir de l'intégration.*

**Faire référence à la base de donnée utilisée dans le fichier web.xml**
(Feng_Nguyen\prototype\Ortho_Spring\Ortho_Spring\src\main\webapp\WEB-INF\web.xml)

```
  <!-- Declaration de l'utilisation de la ressource JDBC -->
    <resource-ref>
        <description>Ressource JDBC de l'application</description>
        <res-ref-name>jdbc/dsMonApplication</res-ref-name>
        <res-type>javax.sql.DataSource</res-type>
        <res-auth>Container</res-auth>
        <res-sharing-scope>Shareable</res-sharing-scope>
    </resource-ref>
```
** Dans le fichier dispatch-servlet.xml (Feng_Nguyen\prototype\Ortho_Spring\Ortho_Spring\src\main\webapp\WEB-INF) :** 

**Déclarer les packages utilisés:**
(ici nos deux jars controller et dao commencent par ortho.spring )

`**<context:component-scan base-package="ortho.spring" />**`


**Déclarer la base Sqlite:**

```
      <bean id="dataSource" class="org.springframework.jdbc.datasource.DriverManagerDataSource" autowire="byName">
        <property name="driverClassName" value="org.sqlite.JDBC"></property>
        <property name="url" value="jdbc:sqlite:E:/java/JavaAvance/OrthoPro/Feng_Nguyen/OrthoPro/orthopro.db"></property>
    </bean>
```

    

** Les composant Spring qui gère la connexion et les transaction à la base:**

```
<!-- Declaration du composant créant les classe associée aux tables -->
 <bean id="entityManagerFactory" class="org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean">
        <property name="dataSource" ref="dataSource" />
    </bean>
```
```
<!-- Declaration du composant gérant les transaction -->
    <bean id="transactionManager" class="org.springframework.orm.jpa.JpaTransactionManager">
        <property name="entityManagerFactory" ref="entityManagerFactory" />
    </bean>
```

**NOTE: **

1- J'ai du déclarer la base Sqlite aussi dans le contexte de Tomcat.

2- Il existe d'autre fonctionnalités comme la facilité de nommage des objets jndiName mais cela ne fonctionne pas pour un problème de compatibilité avec d'autres composants.

** Regardons du côté Java: 3 classes sont nécessaires:**

C'est Spring qui instancie ces trois classes. Le développeur ne fait que développer son contenu.


** Création de la class d'entités associée à la table GRAMMAR:**
On remarque les entité @Entity qui sera utilisé par Spring pour reconnaître ces classes Dao.

'''
@Entity
@Table(name = "GRAMMAR")
public class Grammar {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID")
	private Integer id;
	private String sentence;
	private String difficultytype;
	private String tip;
	private String correction;
	private String advice;

	public Integer getId() {
		return id;
	}

	public void setId(final Integer pId) {
		id = pId;
	}

	public String getSentence() {
		return sentence;
	}

	public void setSentence(final String pSentence) {
		sentence = pSentence;
	}

	public String getDifficultytype() {
		return difficultytype;
	}

	public String setDifficultytype(final String pDifficultytype) {
		return difficultytype = pDifficultytype;
	}

	public String getTip() {
		return tip;
	}

	public void setTip(String pTip) {
		this.tip = pTip;
	}

	public String getCorrection() {
		return correction;
	}

	public void setCorrection(String pCorrection) {
		this.correction = pCorrection;
	}

	public String getAdvice() {
		return advice;
	}

	public void setAdvice(String advice) {
		this.advice = advice;
	}

}'''

 ** Création du service:**
Cette classe contiendra la logique du métier.
Elle est répérée par Spring par l'annotation @Service
On remarque aussi la transaction ici est read-only

'''
@Service
public class ServiceGrammarList implements IServiceGrammarList  {

	 @Autowired
	    private IGrammarListDAO dao;

	    @Transactional(readOnly=true)
	  public List<Grammar> findSentences() {
	    	List<Grammar> grammars = dao.findSentences();
	    	return grammars;
			
		}

}'''

 ** Création du controller:**
On utilise ici @RestController pour utilisation du format Json

```
@RestController
public class MainController {

	 @Autowired
	 private IServiceGrammarList service;
    
   	 
   @RequestMapping(value = "/grammars/{sentence}", produces = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.GET)
	public Grammar getSentence(@PathVariable String sentence) {
		Grammar grammar = service.findSentence(sentence);
		return grammar;
	}
}
```

 ** Test Unitaire: gestion transaction simplifiée**

Un des enjeux des tests unitaires était de pouvoir annuler les modification faites dans la base pendant les tests pour ne pas polluer la base. Avec Spring, il suffit de positionner les annotation "Transactions" et "Rollback" devant les tests.

**4.  VERTX.**

**4.1 Maturité**: 

Vert.x peut être utilisé pour développer des applications Web asynchrones à haute simultanéité et prend en charge une variété de langages de programmation. L'une des caractéristiques les plus importantes de vert.x est asynchrone. Avec les API asynchrones, vous n'avez pas besoin d'attendre que le résultat soit renvoyé et vert.x vous avertira de manière proactive lorsque les résultats sont renvoyés. De plus, l'API asynchrone n'est pas une implémentation multi-thread.
Vert.x Api s'appuie sur les événements. Cela signifie que lorsque Vert.x vous intéresse, Vert.x vous envoie des événements par rappel. (Par exemple le serveur reçoit la requête)

** 4.2 Déclarer les dépendances **: 

(Feng_Nguyen\prototype\Ortho_Spring\Ortho_Spring\pom.xml )

Nous utilisons la version 3.5:

        	<dependency>
	      <groupId>io.vertx</groupId>
	      <artifactId>vertx-core</artifactId>
	      <version>3.5.0</version>
	    </dependency>

	    <dependency>
	      <groupId>io.vertx</groupId>
	      <artifactId>vertx-web</artifactId>
	      <version>3.5.0</version>
	    </dependency>


** 4.3 Créer une Verticle **: 

Verticle est l'unité d'exécution de Vert.x. Différentes langues de programmation ont différentes façons d'implémenter la Verticle, par exemple pour Java nous devons hériter d'une classe abstraite AbstractVerticle. Après le déploiement de la Verticle, le programme appelle la méthode Start, la méthode Start reçoit les paramètres de l'objet Future, vous pouvez dire que l'utilisateur est terminé ou signalé une erreur, Vert.x est exécuté de manière asynchrone, l'exécution n'attend pas le démarrage méthode est terminée, grâce à nous pouvons savoir si la mise en œuvre a été achevée à partir du paramètre Future.
		
	public class MyFirstVerticle extends AbstractVerticle{
		@Override
		public void start(Future<Void> fut) {
	        // Create a router object.
	        Router router = Router.router(vertx);
	
	        // Bind "/" to our hello message.
	        router.route("/").handler(routingContext -> {
	            HttpServerResponse response = routingContext.response();
	            response
	                    .putHeader("content-type", "text/html")
	                    .end("<h1>OrthoPro</h1>");
	        });
	
	        // Create the HTTP server and pass the "accept" method to the request handler.
	        vertx
	                .createHttpServer()
	                .requestHandler(router::accept)
	                .listen(
	                        // Retrieve the port from the configuration,
	                        // default to 8080.
	                        config().getInteger("http.port", 8080),
	                        result -> {
	                            if (result.succeeded()) {
	                                fut.complete();
	                            } else {
	                                fut.fail(result.cause());
	                            }
	                        }
	                );
	    		}
	    	}

Nous créons un objet Router dans la méthode start. Le routeur est la base de Vert.x Web, responsable de la distribution des requêtes HTTP aux handlers.


** 4.4 Implémenter l'API REST à l'aide de Vert.x Web **:

	•	GET /grammars => Obtenir toutes les grammaires（getAll）
	•	GET /grammars/:sentence => Obtenir les grammaires correspondantes（getOne）

    router.route("/assets/*").handler(StaticHandler.create("assets"));
    router.get("/grammars").handler(this::getAll);
    router.get(“/grammars/:sentence").handler(this::getOne);
        
        
    private void getOne(RoutingContext routingContext) {
        final String sentence = routingContext.request().getParam("sentence");
        if (sentence == null) {
            routingContext.response().setStatusCode(400).end();
        } else {
            Grammar grammar = products.get(sentence);
            if (grammar == null) {
                routingContext.response().setStatusCode(404).end();
            } else {
                routingContext.response()
                        .putHeader("content-type", "application/json; charset=utf-8")
                        .end(Json.encodePrettily(grammar));
            }
        }
    }


    private void getAll(RoutingContext routingContext) {
        routingContext.response()
                .putHeader("content-type", "application/json; charset=utf-8")
                .end(Json.encodePrettily(products.values()));
    }


**5.  CONCLUSION.**

Après avoir réalisé les deux prototypes, nous constatons que l'intégration de Vertx semble être  plus facile que Spring. 
Cependant, nous choisissons sans hésiter Spring pour poursuivre notre projet car cette technologie:
- réponds à nos besoins
- Et par rapport à Vertix, elle bénéficie une meilleure popularité. Et de ce point de vue , nous pensons que son apprentissage nous servira mieux dans l'avenir, même si nous sommes conscients qu'elle sera plus complexe à intégrer que VertX.

 
 
