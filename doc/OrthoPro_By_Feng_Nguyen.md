﻿
 **ORTHOPRO**  
==========
 **SPECIFICATION     GENERALE / DETAILLEE** 
==========






 Auteurs                        | Date           |      Version
 -----------------              | -------------- | ------------
 Feng Yu Heng et Nguyen Nam                             |02/10/2017 | V1



##__TABLE DE MATIERES__

[TOC]



__§ REFERENCE__
---



 1.  [Présentation des besoins] 
(https://drive.google.com/file/d/0B4TRjmT1MGdZM0dKMEoyTEdqVmM/view?usp=sharing) du professeur R.FORAX
### 
 __I. OBJECTIF__
--

L'objectif de ce document est de spécifier de façon générale puis détaillée les fonctionnalités d' OrthoPro, une application d'apprentissage de l'orthographe française. Cette application se veut simple à utiliser, pédagogique et évolutive.

### 
 __II. LISTE DES FONCTIONNALITES__
--
1. **Formats en entrée supportés**: texte et audio
--
L'utilisateur peut, soit écrire un texte soit charger un fichier audio ou même, parler dans le micro de son ordinateur; L'application retranscrira le texte audio en texte écrit tel qu'il a été lu.

2. Correction des fautes d'orthographe: mode express ou semi-auto
--
L'application repère alors les fautes d'orthographe à partir des différents supports en entrée. Deux modes de correction sont possibles:
2.1 **Mode express**: toutes les fautes sont automatiquement corrigées. Les correction effectuées sont alors surlignées.
2.2 **Mode semi-auto**: les fautes sont seulement surlignées dans le texte, accompagnées par une proposition de correction.

Dans les deux cas, l'utilisateur a la possibilité de lire un rappel des règles de grammaire en rapport avec la correction effectuée.

 1. **Gestion du profil orthographique**
--

 Un profil d'orthographique sera évalué et affiché après une session d'utilisation 

 2. **Entraînement dictées**
--
Outre les correction d'un texte, l'application propose une série de dictées permettant à l'utilisateur de s'entrainer à l'orthographe.
4.1 Des exercices de dictées sont proposées via des textes audio. 
4.2 L'utilisateur pourra choisir le niveau de difficulté de l'exercice.
  
 3. **Paramétrage des règles de grammaire et les phrases-types**
--
5.1 Les règles de grammaire et une liste des phrases dont l'application est capable de corriger, seront fournies sous la forme de fichier au format csv. 
5.2 L'application ne peut corriger que les phrases qui existent dans cette base de grammaire. 
5.3 ***Evolution***: dans cette version, les phrases supportées portent sur un domaine spécifique:  le génie civil. Cependant, l'utilisateur pourra enrichir cette base de grammaire pour l'étendre vers d'autres domaines. 
 4. **Contraintes techniques**
--
6.1 **Interface web**: L'application sera accessible via une interface Web dans cette version. 
6.2. **Indépendance graphique**: l'implémentation technique doit être faite de telle façon que l'ajout d'un autre support (type client lourd, mobile etc...) n'entraînera pas de modification dans le code existant (du moins, pas importantes. Restons pragmatique. ) .
6.3. **Indépendance base fichier**: la base de grammaire est stockées sous la forme de fichier dans cette version. Cependant,  l'architecture doit être assez souple pour supporter, sans tout remettre en cause, une migration éventuelle vers une autre type base (relationnelle classique, Mongoldb..etc).  
6.4. **Accessible clavier**: toute manipulation doit pouvoir être faite avec le clavier (touches raccourcies) aussi bien qu'avec la souris.
### 
__III. DESCRIPTION DU FONCTIONNEMENT:  INTUTIF POUR TOUT PUBLIC.__
--
 Nous allons décrire la façon les utilisateurs pourront utiliser les fonctionnalités de l'application, à travers son interface graphique.
 1. Vue globale des écrans
 --
 
![enter image description here](https://lh3.googleusercontent.com/-0a2Q1kTb9zU/Wdhy0G02bbI/AAAAAAAAAgc/ycYB1yZIefkhxn4LYH02lnvgqGbSL0FFQCLcBGAs/s0/ALL_ECRAN.jpg "Vue Globale des écrans")

2. Ecran d'entrée: pour utilisation complète avec login ou une utilisation rapide sans login.
--
![enter image description here](https://lh3.googleusercontent.com/-CNBdKrwzZgA/Wdhzval4JCI/AAAAAAAAAgo/M3NykAOtf34LgzoRBL9juG51cZBc4T1UACLcBGAs/s0/Home.page.jpg "Home.page.jpg")

3. Effectuer une correction d'un texte écrit /audio: accessible libre sans login
--
![enter image description here](https://lh3.googleusercontent.com/-stx8pPh0SX4/Wdh0Oz-2ZkI/AAAAAAAAAhA/M7xRVNTgE7EPuQDqSomftzVuLSFtigm0gCE0YBhgL/s0/main.page.jpg "main.page.jpg")

4. Se connecter (pour bénéficier des toutes les fonctionnalités de l'application).
--


 ![Connexion](https://lh3.googleusercontent.com/-mebGW31aFPw/Wdh1PePJ2AI/AAAAAAAAAhc/Jse5j2ZS2L8EIYeYC5ResuNXbZZ6MkCwACLcBGAs/s0/Login.page.jpg "Login") 

5. Effectuer des dictées (après un login):
--

![enter image description here](https://lh3.googleusercontent.com/-cCuN95xM6OM/Wdh3GGXd3cI/AAAAAAAAAh4/E2lxVuzNCAoq6RvHuEHhFNIESwZhQkafACLcBGAs/s0/User.page.jpg "User.page.jpg")


5. Statistiques et paramétrage (après login)
--
![enter image description here](https://lh3.googleusercontent.com/-G8s20-zi57E/Wdh2cNNqkgI/AAAAAAAAAhs/9h8Ewf9NBScat6B3KhjWj33P71kGx7WuwCLcBGAs/s0/Profil.page.jpg "Profil.page.jpg")