
**Main Fonction (Écran principale)**

**Corriger d'un texte**

GET  /grammars/:text

Réponse:
```json
{
    "$schema": "http://json-schema.org/draft-06/schema#",
    "title": "Sentence",
    "description": "resultat de la correction d'une phrase",
    "type": "object",
    "properties": {
	  "id": {
            "description": "identifiant",
            "type": "string"
        },
        "sentence": {
            "description": "phrase incorrecte",
            "type": "string"
        },
        "difficultytype": {
            "description": "difficult ytype",
            "type": "string"
        },
        "hint": {
            "description": "un indice de correction",
            "type": "string"
            }
        },
        "correction": {
            "description": "phrase incorrecte",
            "type": "string"
        },
        "advice": {
            "description": "phrase incorrecte",
            "type": "string"
            }
        }
    },
    "audio": {
            "description": "audio",
            "type": "string"
            }
        },

       "info": {
            "description": "details about correction result",
            "type": "string"
        }, 
     "isCorrect": {
            "description": "the sentence is correct or not",
            "type": "boolean"
        }, 

    "required": ["id", "sentence", "correction"]
}
```
Exemple: 
```json
[   {
    "grammar": {
        "id": 1,
        "sentence": "PleinFauteOrtho",
        "difficultytype": "testdifficultytype",
        "hint": null,
        "correction": "Plein de fautes",
        "advice": null,
        "audio": null
    },
    "info": "",
    "correct": false
}
]

```

--------------

**Récupérer toutes les grammaires**

GET  /grammars

Réponse:

```json
[
    {
        "id": 1,
        "sentence": "PleinFauteOrtho",
        "difficultytype": "testdifficultytype",
        "hint": null,
        "correction": "Plein de fautes",
        "advice": null,
        "audio": null
    },
    {
        "id": 2,
        "sentence": "L'entreprise a procédé au désamiantage suite a la demande du maître d'ouvrage. ",
        "difficultytype": "Homophones a/à",
        "hint": "avait ou non ?",
        "correction": "L'entreprise a procédé au désamiantage suite à la demande du maître d'ouvrage. ",
        "advice": "Penser à essayer \"avait\" dès qu'on s'apprête à écrire \"a\" tout seul ! \n\nSi \"avait\" peut remplacer a dans la phrase , pas d'accent. \nSinon : un",
       "audio": "SUQzBAAAAAABR1RYWFgAAAASAAADbW"
       },
    {
        "id": 3,
        "sentence": "Mondeuxitest",
        "difficultytype": "testdifficultytype",
        "hint": "écrire lentement",
        "correction": "mon deuxième test",
        "advice": "metre espace et arrêter le langage de chat",
        "audio": null
    },
    {
        "id": 4,
        "sentence": "Cestmon troisitest",
        "difficultytype": "niveau2",
        "hint": "arrêter taper",
        "correction": "C'est mon troisième test",
        "advice": "exercer le niveau 2",
        "audio":"//tSxAAAAAABpAAAAAAAADSCgA.."
        },
    {
        "id": 5,
        "sentence": "Cestmon quatitest",
        "difficultytype": "niveau2",
        "hint": "quati n'existe pas",
        "correction": "C'est mon quatrième test",
        "advice": "exercer le niveau 2",
        "audio": "/+MYxAAAAANIAAAAAAFH9ZLdtkUBAMH+f2"
        }
]
  ------

**Récupérer une série de grammaires pour un niveau de difficulté donnée **

GET  /dictation/:diffcultytype

Exemple

GET /dictation/niveau2

Réponse:

```json
[
    {
        "id": 4,
        "sentence": "Cestmon troisitest",
        "difficultytype": "niveau2",
        "hint": "arrêter taper",
        "correction": "C'est mon troisième test",
        "advice": "exercer le niveau 2",
        "audio": "//tSxAAAAAABpAAAAAAAAD"
    },
    {
        "id": 5,
        "sentence": "Cestmon quatitest",
        "difficultytype": "niveau2",
        "hint": "quati n'existe pas",
        "correction": "C'est mon quatrième test",
        "advice": "exercer le niveau 2",
        "audio": "/+MYxAAAAANIAAAAAAFH9ZLdt"
     }
]

```

--------------

**Upload 1 série de grammaire**

POST  /grammars/:{multipartFile}

Réponse:

```json

{
    "$schema": "http://json-schema.org/draft-06/schema#",
    "title": "Sentence",
    "description": "les phrase à uploader",
    "type": "object",
    "properties": {
      "fileName": {
            "description": "fichier .csv, champs séparés par |",
            "type": "byte"
        },
        "size": {
            "description": "file size",
            "type": "number"
        }
    "required": ["size", "number"]
}
```
Exemple

POST /grammars/"myTestFile"

Réponse:

  {
        "fileName": myTestFile,
        "size": "100"
   }

--------------

**Upload un fichier audio dans la base pour une grammaire donnée**

POST  /grammar/audioFile=:file, grammarId=:grammarId, 

Réponse:

```json

{
    "$schema": "http://json-schema.org/draft-06/schema#",
    "title": "Sentence",
    "description": "dictée",
    "type": "object",
    "properties": {
      "audioFile": {
            "description": "fichier audio",
            "type": "byte"
        },
        "size": {
            "description": "file size",
            "type": "number"
        }
    "required": ["audioFile", "size"]
}
```
Exemple

POST /grammars/audioFile="3etestmp3", grammarId=4

Réponse:

  {
        "fileName": 3etestmp3,
        "size": 38687,
  }


