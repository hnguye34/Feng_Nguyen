<%@ page language="java" contentType="text/html; charset=ISO-8859-1" isELIgnored="false"
    pageEncoding="ISO-8859-1"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <title><spring:message code="titre.sentences"/></title>
    </head>
    <body>
        <table border="1">
            <thead>
                <tr>
                    <th><spring:message code="colonne.identifiant"/></th>
                    <th><spring:message code="colonne.sentence"/></th>
                    <th><spring:message code="colonne.difficultyType"/></th>
                     <th><spring:message code="colonne.hint"/></th>
                      <th><spring:message code="colonne.correction"/></th>
                       <th><spring:message code="colonne.advice"/></th>
                </tr>
            </thead>
            <tbody>
                <c:forEach items="${sentenceList}" var="grammar">
                    <tr>
                        <td><c:out value="${grammar.id}"/></td>
                        <td><c:out value="${grammar.sentence}"/></td>
                         <td><c:out value="${grammar.difficultytype}"/></td>
                         <td><c:out value="${grammar.tip}"/></td>
                        <td><c:out value="${grammar.correction}"/></td>
                        <td><c:out value="${grammar.advice}"/></td>
                    </tr>
                </c:forEach>
            </tbody>
        </table>
    </body>
</html>