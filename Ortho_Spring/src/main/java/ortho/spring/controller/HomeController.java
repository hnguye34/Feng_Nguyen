package ortho.spring.controller;

import org.springframework.stereotype.Controller;


import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class HomeController {

	@RequestMapping("/")
	public String index() {
		//ModelAndView modelAndView = new ModelAndView("index");
		return "index";
	}
	
	@RequestMapping("/index")
	public String index2() {
		return "index";
	}
	
	@RequestMapping("/correction")
	public String correction() {
		return "correction";
	}
	
	@RequestMapping("/dictation")
	public String dictation() {
		return "dictation";
	}
}
