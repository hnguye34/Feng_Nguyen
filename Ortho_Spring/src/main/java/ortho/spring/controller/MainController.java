package ortho.spring.controller;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.util.List;
import java.util.regex.Pattern;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import ortho.spring.dao.IServiceGrammarList;
import ortho.spring.dao.SvcCorrectionResult;
import ortho.spring.dao.Grammar;

@RestController
public class MainController {

	@Autowired
	private IServiceGrammarList service;

	public MainController(IServiceGrammarList userService) {
		service = userService;
	}

	@GetMapping(path = "grammars", produces = MediaType.APPLICATION_JSON_VALUE)
	public List<Grammar> afficher( /* ModelMap pModel */) {

		final List<Grammar> grammars = service.findSentences();
		return grammars;
	}

	@RequestMapping(value = "/dictation/{difficulty}", produces = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.GET)
	public List<Grammar> getSentenceByDifficulty(@PathVariable String difficulty) {
		List<Grammar> grammars = service.findSentencesBydifficulty(difficulty);
		return grammars;
	}

	@RequestMapping(value = "/grammars/{sentence}",
			 produces = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.GET)
	public SvcCorrectionResult getCorrection(@PathVariable String sentence) {
	
		SvcCorrectionResult svcCorrectionResult = service.isCorrectSentence( sentence);
		return svcCorrectionResult ;
		
	}

	@RequestMapping(value = "/grammars", /*headers = ("content-type=multipart/*"), */ method = RequestMethod.POST)
	public ResponseEntity<FileInfo> uploadGrammars(@RequestParam("inputFile") MultipartFile inputFile) throws IOException {
		FileInfo fileInfo = new FileInfo();
		HttpHeaders headers = new HttpHeaders();
		if (!inputFile.isEmpty()) {

			String originalFilename = inputFile.getOriginalFilename();

			fileInfo.setFileName(originalFilename);
			fileInfo.setFileSize(inputFile.getSize());
			try (InputStream inputStream = inputFile.getInputStream()) {
				BufferedReader br = new BufferedReader(new InputStreamReader(inputStream));

				br.lines().filter(line -> !line.isEmpty()).forEach(line -> parseLine(line));
				
				 headers.add("File Uploaded Successfully - ", originalFilename);
				 return new ResponseEntity<FileInfo>(fileInfo, headers, HttpStatus.OK);
			} 

		}

		return new ResponseEntity<FileInfo>(fileInfo, headers, HttpStatus.BAD_REQUEST);
	}
	
	@RequestMapping(value = "/grammar",  method = RequestMethod.POST)
	public ResponseEntity<FileInfo> uploadReadSentence(@RequestParam("audioFile") MultipartFile inputFile,
													  @RequestParam("grammarId")  String grammarId
			
				) throws IOException {
		FileInfo fileInfo = new FileInfo();
		HttpHeaders headers = new HttpHeaders();
		if (!inputFile.isEmpty()) {

			String originalFilename = inputFile.getOriginalFilename();

			fileInfo.setFileName(originalFilename);
			fileInfo.setFileSize(inputFile.getSize());
			try (InputStream inputStream = inputFile.getInputStream()) {

				 Grammar grammar = service.updateAudio(Integer.parseInt(grammarId), inputStream.readAllBytes());
				 
				 if ( grammar == null )
					 return new ResponseEntity<FileInfo>(fileInfo, headers, HttpStatus.BAD_REQUEST);
				 
				 headers.add("File Uploaded Successfully - ", originalFilename);
				 return new ResponseEntity<FileInfo>(fileInfo, headers, HttpStatus.OK);
			} 

		}

		return new ResponseEntity<FileInfo>(fileInfo, headers, HttpStatus.BAD_REQUEST);
	}

	

	private void parseLine(String line) {

		String[] fields = line.split(Pattern.quote("|"));

		if (fields.length < 5 || fields.length > 6 )
			return;

		if (fields[0].length() > 0 && fields[3].length() > 0)
			service.addGrammar(fields[0], fields[1], fields[2], fields[3], fields[4]);

	}

}