package ortho.spring.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import org.springframework.stereotype.Repository;

@Repository
public class GrammarListDAO implements IGrammarListDAO {
	@PersistenceContext
	private EntityManager entityManager;

	public List<Grammar> findSentences() {
		CriteriaBuilder lCriteriaBuilder = entityManager.getCriteriaBuilder();

		CriteriaQuery<Grammar> lCriteriaQuery = lCriteriaBuilder.createQuery(Grammar.class);
		Root<Grammar> lRoot = lCriteriaQuery.from(Grammar.class);
		lCriteriaQuery.select(lRoot);
		TypedQuery<Grammar> lTypedQuery = entityManager.createQuery(lCriteriaQuery);

		return lTypedQuery.getResultList();
	}

	public Grammar findAGrammarBySentence(String sentence) {
		CriteriaBuilder lCriteriaBuilder = entityManager.getCriteriaBuilder();

		CriteriaQuery<Grammar> lCriteriaQuery = lCriteriaBuilder.createQuery(Grammar.class);
		Root<Grammar> lRoot = lCriteriaQuery.from(Grammar.class);
		Expression<String>  sentenceClause = lRoot.get("sentence");
		Predicate whereClause = lCriteriaBuilder.like(lCriteriaBuilder.lower(sentenceClause), sentence.toLowerCase() + "%"); 
		lCriteriaQuery.where(whereClause);

		TypedQuery<Grammar> lTypedQuery = entityManager.createQuery(lCriteriaQuery);

		lTypedQuery.setMaxResults(1);

		List<Grammar> grammars = lTypedQuery.getResultList() ; 
		
		if (grammars == null || grammars.isEmpty()) {
	        return null;
	    }

		return grammars.get(0);
	}

	public Grammar findAGrammarById(int sentenceId) {
		CriteriaBuilder lCriteriaBuilder = entityManager.getCriteriaBuilder();

		CriteriaQuery<Grammar> lCriteriaQuery = lCriteriaBuilder.createQuery(Grammar.class);
		Root<Grammar> lRoot = lCriteriaQuery.from(Grammar.class);
		lCriteriaQuery.where(lCriteriaBuilder.equal(lRoot.get("id"), sentenceId));

		TypedQuery<Grammar> lTypedQuery = entityManager.createQuery(lCriteriaQuery);
		
		lTypedQuery.setMaxResults(1);
		
		List<Grammar> grammars = lTypedQuery.getResultList() ; 
		
		if (grammars == null || grammars.isEmpty()) {
	        return null;
	    }

		return grammars.get(0);

		
	}
	
	

	public void addGrammar(Grammar grammartoAdd) {
		entityManager.persist(grammartoAdd);

	}

	public List<Grammar> findAGrammarByDifficultyType(String difficultyType) {
		CriteriaBuilder lCriteriaBuilder = entityManager.getCriteriaBuilder();

		CriteriaQuery<Grammar> lCriteriaQuery = lCriteriaBuilder.createQuery(Grammar.class);
		Root<Grammar> lRoot = lCriteriaQuery.from(Grammar.class);
		lCriteriaQuery.where(lCriteriaBuilder.equal(lRoot.get("difficultytype"), difficultyType));

		TypedQuery<Grammar> lTypedQuery = entityManager.createQuery(lCriteriaQuery);

		return lTypedQuery.getResultList();
	}

	@Override
	public Grammar findAGrammarByCorrection(String correction) {
		CriteriaBuilder lCriteriaBuilder = entityManager.getCriteriaBuilder();

		CriteriaQuery<Grammar> lCriteriaQuery = lCriteriaBuilder.createQuery(Grammar.class);
		Root<Grammar> lRoot = lCriteriaQuery.from(Grammar.class);
		Expression<String>  sentenceClause = lRoot.get("correction");
		Predicate whereClause = lCriteriaBuilder.like(lCriteriaBuilder.lower(sentenceClause), correction.toLowerCase()); 
		lCriteriaQuery.where(whereClause);

		TypedQuery<Grammar> lTypedQuery = entityManager.createQuery(lCriteriaQuery);
		
		List<Grammar> grammars = lTypedQuery.getResultList() ; 
		
		if (grammars == null || grammars.isEmpty()) {
	        return null;
	    }

		return grammars.get(0);
		
	}

}
