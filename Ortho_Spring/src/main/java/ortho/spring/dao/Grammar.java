package ortho.spring.dao;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "GRAMMAR")
public class Grammar {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID")
	private Integer id;
	private String sentence;
	private String difficultytype;
	@Column(name = "tip")
	private String hint;
	private String correction;
	private String advice;
	private byte[] audio;

	public Integer getId() {
		return id;
	}

	public void setId(final Integer pId) {
		id = pId;
	}

	public String getSentence() {
		return sentence;
	}

	public void setSentence(final String pSentence) {
		sentence = pSentence;
	}

	public String getDifficultytype() {
		return difficultytype;
	}

	public String setDifficultytype(final String pDifficultytype) {
		return difficultytype = pDifficultytype;
	}

	public String getHint() {
		return hint;
	}

	public void setHint(String pHint) {
		this.hint = pHint;
	}

	public String getCorrection() {
		return correction;
	}

	public void setCorrection(String pCorrection) {
		this.correction = pCorrection;
	}

	public String getAdvice() {
		return advice;
	}

	public void setAdvice(String advice) {
		this.advice = advice;
	}

	public byte[] getAudio() {
		return audio;
	}

	public void setAudio(byte[] audio) {
		this.audio = audio;
	}

}
