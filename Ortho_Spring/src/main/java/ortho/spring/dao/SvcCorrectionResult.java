package ortho.spring.dao;

public class SvcCorrectionResult {
	
	private final Grammar grammar;
	private final boolean isCorrect;
	private final String info ;
	
	public SvcCorrectionResult(Grammar grammar, boolean isCorrect, String info) {
	
		this.grammar = grammar;
		this.isCorrect = isCorrect;
		this.info = info;
	}

	public Grammar getGrammar() {
		return grammar;
	}

	public boolean isCorrect() {
		return isCorrect;
	}

	public String getInfo() {
		return info;
	}
	


}
