package ortho.spring.dao;

import java.util.List;

/**
 * interface IServiceGrammarList providing all functionalities of the application.
 * Intermediate component between the controller and DataAccess 
 *
 */
public interface IServiceGrammarList {
	/** search all available grammars
	 * @return all the grammars
	 */
	List<Grammar> findSentences();
	
	/** search all grammars for a difficulty type
	 * @param difficulty : difficulty level
	 * @return grammars for a given difficulty level
	 */
	List<Grammar> findSentencesBydifficulty( String difficulty);

	/** search a grammar for a sentence containing errors
	 * @param sentence: the sentence with errors
	 * @return all the grammar , null if the sentence does not exist
	 */
	Grammar findSentence(String sentence);
	/** add an audiofile for a given grammar
	 * @param id : grammar id
	 * @param audio : the audio file in mp3 format
	 * @return the modified grammar
	 */
	Grammar updateAudio(int id, byte[] audio );
	
	/** check the sentence is correct and get correction details
	 * @param sentence to correct
	 * @return correction details.
	 */
	SvcCorrectionResult isCorrectSentence(String sentence);

	
	/** add a grammar in database. Used to import new grammars
	 * @param falseSentence : the sentence with errors
	 * @param difficultType : difficulty type
	 * @param hint : a hint to find the answer
	 * @param correctionSentence : the good sentence
	 * @param advice : some advices to improve 
	 */
	void addGrammar(String falseSentence, String difficultType, String hint, String correctionSentence, String advice);

}
