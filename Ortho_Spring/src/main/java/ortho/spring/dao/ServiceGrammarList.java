package ortho.spring.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class ServiceGrammarList implements IServiceGrammarList {

	@Autowired
	private IGrammarListDAO dao;

	public ServiceGrammarList (IGrammarListDAO dao)
	{
		this.dao = dao;
	}
	



	@Override
	public Grammar findSentence(String sentence) {

		Grammar grammar = dao.findAGrammarBySentence(sentence);

		return grammar;
	}

	@Override
	public SvcCorrectionResult isCorrectSentence(String sentence) {

		Grammar grammar = findSentence(sentence);
		
		if (grammar != null )
			return new SvcCorrectionResult (grammar, false, "" ) ;
		else 
		{
			grammar = dao.findAGrammarByCorrection(sentence);
			
			if ( grammar != null )
				return new SvcCorrectionResult (null, true, "Vérification terminée" ) ;
			
			
			return new SvcCorrectionResult (null, false, "Ce texte n'existe pas dans la base de correction." ) ;
		}
		
	}

	@Override
	@Transactional
	public void addGrammar(String falseSentence, String difficultType, String hint, String correctionSentence,
			String advice) {
		Grammar grammar = new Grammar();

		grammar.setSentence(falseSentence);
		grammar.setCorrection(correctionSentence);
		grammar.setAdvice(advice);
		grammar.setDifficultytype(difficultType);
		grammar.setHint(hint);

		dao.addGrammar(grammar);

	}
	
	@Override
	public List<Grammar> findSentencesBydifficulty(String difficulty) {
		List<Grammar> grammars = dao.findAGrammarByDifficultyType(difficulty);
		return grammars;
	}
	
	@Override
	@Transactional
	public Grammar updateAudio(int id, byte[] audio) {
	
		Grammar grammar = dao.findAGrammarById(id) ;
		if ( grammar != null)
			grammar.setAudio(audio);
		
		dao.addGrammar(grammar);
		
		return grammar;
	}



	
	@Override
	public List<Grammar> findSentences() {
		List<Grammar> grammars = dao.findSentences();
		return grammars;

	}




}
