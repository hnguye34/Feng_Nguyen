package ortho.spring.dao;

import java.util.List;

/**
 * interface IGrammarListDAO providing the updating functions of the database
 * Low level component, used by the service component  
 */

public interface IGrammarListDAO {
	
	/**
	 * @return all grammars from the database
	 */
	List<Grammar> findSentences();

	/**
	 * @param sentence : uncorrect sentence to search 
	 * @return a grammar associated with the sentence to correct
	 */
	Grammar findAGrammarBySentence(String sentence);
	
	/**
	 * @param correction: the correction to search (a correct sentence)
	 * @return a grammar associated with a correction
	 */
	Grammar findAGrammarByCorrection(String correction);

	/**
	 * @param sentenceId : technical id of the grammar
	 * @return a grammar associated from the Id
	 */
	Grammar findAGrammarById(int sentenceId);
	
	/**
	 * @param difficultyType: difficulty type 
	 * @return a grammar from a difficulty type
	 */
	List<Grammar> findAGrammarByDifficultyType(String difficultyType);

	/** 
	 * @param grammartoAdd : grammar to add
	 * add a grammar in base. Used for example to import the grammars
	 */
	void addGrammar(Grammar grammartoAdd);

}