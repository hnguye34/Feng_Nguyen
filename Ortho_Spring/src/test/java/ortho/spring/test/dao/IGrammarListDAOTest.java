package ortho.spring.test.dao;



import java.util.List;

import org.junit.Assert;
//import org.junit.jupiter.api.Test;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;
 


//import org.junit.jupiter.api.Test;

import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;



import ortho.spring.dao.Grammar;
import ortho.spring.dao.IGrammarListDAO;


@ContextConfiguration(locations = { "file:src/test/ressources/dispatcher-servlet-test.xml"})
@RunWith(SpringJUnit4ClassRunner.class)
public class IGrammarListDAOTest {

	@Autowired
	private IGrammarListDAO dao;

	@Test
	@Transactional
	@Rollback(true)
	public void testFindSentences() {

		List<Grammar> grammars = dao.findSentences();

		Assert.assertNotEquals(grammars.size(), 0);
	}

	@Test
	@Transactional
	@Rollback(true)
	public void testFindAGrammarBySentence() {

		String sentence = "PleinfauteOrtho";
		Grammar grammar = dao.findAGrammarBySentence(sentence);

		Assert.assertNotNull(grammar);

		Assert.assertTrue(grammar.getSentence().toLowerCase().contains(sentence.toLowerCase()));
		
	}
	
	@Test
	public void testFindAGrammarByCorrectionAmiante() {

		String correction = "L'entreprise a procédé au désamiantage suite à la demande du maître d'ouvrage.";
		Grammar grammar = dao.findAGrammarByCorrection(correction);

		Assert.assertNotNull(grammar);

		Assert.assertTrue(grammar.getCorrection().equals(correction));
	}
	
	@Test
	public void testFindAGrammarByCorrection() {

		String correction = "mon deuxième test";
		Grammar grammar = dao.findAGrammarByCorrection(correction);

		Assert.assertNotNull(grammar);

		Assert.assertTrue(grammar.getCorrection().toLowerCase().contains(correction.toLowerCase()));
	}

	@Test
	public void testFindAGrammarById() {

		Grammar grammar = dao.findAGrammarById(1);

		Assert.assertNotNull(grammar);

		Assert.assertTrue(grammar.getSentence().compareToIgnoreCase("PleinfauteOrtho") == 0);

	}
	@Test
	@Transactional
	@Rollback(true)
	public void testFindAGrammarByDifficultyType() { 
		List<Grammar> grammars = dao.findAGrammarByDifficultyType("Homophones a/à");

		Assert.assertNotNull(grammars);
		Assert.assertTrue(!grammars.isEmpty());

	}

	@Test
	@Transactional
	@Rollback(true)
	public void testAddGrammar() {
		Grammar grammar = new Grammar();

		//grammar.setId(100);
		String incorrectSentence = "mes fautes";
		grammar.setSentence(incorrectSentence);
		
		grammar.setCorrection("mes fautes");
		grammar.setAdvice("my advice");
		grammar.setCorrection("my correction");
		grammar.setDifficultytype("Tres dur");
		grammar.setHint("Deviner");

		dao.addGrammar(grammar);

		Grammar checkGrammar = dao.findAGrammarBySentence(incorrectSentence);

		Assert.assertNotNull(checkGrammar);

		Assert.assertTrue(checkGrammar.getSentence().equals(grammar.getSentence()));
		
		Assert.assertTrue(checkGrammar.getCorrection().equals(grammar.getCorrection()));

	}

}
