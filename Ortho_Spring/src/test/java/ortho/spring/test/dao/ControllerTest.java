package ortho.spring.test.dao;



import org.junit.Test;
import org.junit.Before;
//import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.WebApplicationContext;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.io.IOException;
import java.io.InputStream;


import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;



@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration(locations = { "file:src/test/ressources/dispatcher-servlet-test.xml" })
public class ControllerTest {

	@Autowired
	private WebApplicationContext wac;

	// @Autowired
	private MockMvc mockMvc;

	@Before
	public void setup() {
		// MockitoAnnotations.initMocks(this);
		// this.mockMvc = MockMvcBuilders.standaloneSetup(userController).build();
		this.mockMvc = MockMvcBuilders.webAppContextSetup(this.wac).build();

	}

	@Test
	public void testGetSentence() throws Exception {
		String sentence = "PleinfauteOrtho";
		String correction = "Plein de fautes";

		ResultActions result = mockMvc.perform(get("/grammars/{sentence}", sentence));
		result.andExpect(status().isOk())
			   .andExpect(jsonPath("$.grammar.correction").value(correction));

	}
	
	@Test
	public void testGetCorrectionSentence() throws Exception {
		String correction = "Plein de fautes";

		ResultActions result = mockMvc.perform(get("/grammars/{sentence}", correction));
		result.andExpect(status().isOk())
			   .andExpect(jsonPath("$.info").value("Vérification terminée"));

	}
	
	
	public void testWithSennteceAmianteIsCorrect() throws Exception {
		String correction = "L'entreprise a procédé au désamiantage suite à la demande du maître d'ouvrage.";

		ResultActions result = mockMvc.perform(get("/grammars/{sentence}", correction));
		result.andExpect(status().isOk())
			   .andExpect(jsonPath("$.info").value("Vérification terminée"));

	}
	
	@Test
	public void testSentenceNotExist() throws Exception {
		String sentence = "NotExist";

		ResultActions result = mockMvc.perform(get("/grammars/{sentence}", sentence));
		result.andExpect(status().isOk())
			   .andExpect(jsonPath("$.info").value("Ce texte n'existe pas dans la base de correction." ));

	}

	@Test
	public void testGetSentenceByDifficulty() throws Exception {
		String difficulty = "niveau2";

		mockMvc.perform(get("/dictation/{difficulty}", difficulty)).andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
				.andExpect(jsonPath("$[0].correction").value("C'est mon troisième test"))
				.andExpect(jsonPath("$[0].sentence").value("Cestmon troisitest"))
				.andExpect(jsonPath("$[0].hint").value("arrêter taper"))
				.andExpect(jsonPath("$[0].advice").value("exercer le niveau 2"))
				.andExpect(jsonPath("$[1].correction").value("C'est mon quatrième test"))
				.andExpect(jsonPath("$[1].sentence").value("Cestmon quatitest"))
				.andExpect(jsonPath("$[1].hint").value("quati n'existe pas"))
				.andExpect(jsonPath("$[1].advice").value("exercer le niveau 2"));

	}

	@Test
	@Transactional
	@Rollback(true)
	public void testUploadGrammars() throws Exception {
		String mygrammar = "FauteEtFaute|MyDifficultyLevel|Des espaces|des fautes et encore des fautes| Il faut mettre l'article";
		String fileName = "myTestFile";
		MockMultipartFile myTestFile = new MockMultipartFile("inputFile", fileName, null, mygrammar.getBytes());
		long size = myTestFile.getSize();
		mockMvc.perform(MockMvcRequestBuilders.multipart("/grammars").file(myTestFile)).andExpect(status().isOk())
				.andExpect(jsonPath("$.fileName").value(fileName)).andExpect(jsonPath("$.fileSize").value(size));

	}

	@Test
	@Transactional
	@Rollback(true)
	public void testUploadAudioGrammar() throws Exception {

		//TODO: chercher pk gitlab ne trouve pas ces fichiers alors que c'est ok en local
		//updateAudio("desamiantage.mp3", 2);
		//updateAudio("4e.mp3", 5);
		updateAudio("3etest.mp3", 4);

	}

	private void updateAudio(String audioFileName, int grammarId) throws IOException, Exception {
		ClassPathResource resource = new ClassPathResource(audioFileName);

		try (InputStream inputStream = resource.getInputStream()) {
		
			MockMultipartFile desamiantageFile = new MockMultipartFile("audioFile", audioFileName, null,
					inputStream.readAllBytes());
			long size = desamiantageFile.getSize();

			mockMvc.perform(MockMvcRequestBuilders.multipart("/grammar").file(desamiantageFile).param("grammarId", String.valueOf(grammarId)))
					.andExpect(status().isOk()).andExpect(jsonPath("$.fileName").value(audioFileName))
					.andExpect(jsonPath("$.fileSize").value(size));
		}
	}

}
