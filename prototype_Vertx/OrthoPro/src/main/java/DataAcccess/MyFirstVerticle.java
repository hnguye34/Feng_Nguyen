package DataAcccess;


import io.vertx.core.AbstractVerticle;
import io.vertx.core.Future;
import io.vertx.core.http.HttpServerResponse;
import io.vertx.core.json.Json;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.handler.BodyHandler;
import io.vertx.ext.web.handler.StaticHandler;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * This is a verticle. A verticle is a _Vert.x component_. This verticle is implemented in Java, but you can
 * implement them in JavaScript, Groovy or even Ruby.
 */
public class MyFirstVerticle extends AbstractVerticle {

    private Map<Integer, Grammar> products = new LinkedHashMap<>();

    /**
     * This method is called when the verticle is deployed. It creates a HTTP server and registers a simple request
     * handler.
     * <p/>
     * Notice the `listen` method. It passes a lambda checking the port binding result. When the HTTP server has been
     * bound on the port, it call the `complete` method to inform that the starting has completed. Else it reports the
     * error.
     *
     * @param fut the future
     */
    @Override
    public void start(Future<Void> fut) {

        createSomeData();

        // Create a router object.
        Router router = Router.router(vertx);

        // Bind "/" to our hello message.
        router.route("/").handler(routingContext -> {
            HttpServerResponse response = routingContext.response();
            response
                    .putHeader("content-type", "text/html")
                    .end("<h1>Test VertX</h1>");
        });

        router.route("/assets/*").handler(StaticHandler.create("assets"));

        router.get("/api/grammar").handler(this::getAll);
        router.post("/api/grammar").handler(this::addOne);
        router.get("/api/grammar/:sentence").handler(this::getOne);
        router.put("/api/grammar/:sentence").handler(this::updateOne);
        router.delete("/api/grammar/:sentence").handler(this::deleteOne);


        // Create the HTTP server and pass the "accept" method to the request handler.
        vertx
                .createHttpServer()
                .requestHandler(router::accept)
                .listen(
                        // Retrieve the port from the configuration,
                        // default to 8080.
                        config().getInteger("http.port", 8080),
                        result -> {
                            if (result.succeeded()) {
                                fut.complete();
                            } else {
                                fut.fail(result.cause());
                            }
                        }
                );
    }

    private void addOne(RoutingContext routingContext) {
        final Grammar grammar = Json.decodeValue(routingContext.getBodyAsString(),
        		Grammar.class);
        products.put(grammar.getId(), grammar);

        routingContext.response()
                .setStatusCode(201)
                .putHeader("content-type", "application/json; charset=utf-8")
                .end(Json.encodePrettily(grammar));
    }

    private void getOne(RoutingContext routingContext) {
        final String sentence = routingContext.request().getParam("sentence");
        if (sentence == null) {
            routingContext.response().setStatusCode(400).end();
        } else {
            Grammar grammar = products.get(sentence);
            if (grammar == null) {
                routingContext.response().setStatusCode(404).end();
            } else {
                routingContext.response()
                        .putHeader("content-type", "application/json; charset=utf-8")
                        .end(Json.encodePrettily(grammar));
            }
        }
    }

    private void updateOne(RoutingContext routingContext) {
        final String sentence = routingContext.request().getParam("sentence");
        JsonObject json = routingContext.getBodyAsJson();
        if (sentence == null || json == null) {
            routingContext.response().setStatusCode(400).end();
        } else {
            Grammar grammar = products.get(sentence);
            if (grammar == null) {
                routingContext.response().setStatusCode(404).end();
            } else {
            	grammar.setSentence(json.getString("sentence"));
            	grammar.setCorrection(json.getString("correction"));
                routingContext.response()
                        .putHeader("content-type", "application/json; charset=utf-8")
                        .end(Json.encodePrettily(grammar));
            }
        }
    }

    private void deleteOne(RoutingContext routingContext) {
        String sentence = routingContext.request().getParam("sentence");
        if (sentence == null) {
            routingContext.response().setStatusCode(400).end();
        } else {
            products.remove(sentence);
        }
        routingContext.response().setStatusCode(204).end();
    }

    private void getAll(RoutingContext routingContext) {
        // Write the HTTP response
        // The response is in JSON using the utf-8 encoding
        // We returns the list of bottles
        routingContext.response()
                .putHeader("content-type", "application/json; charset=utf-8")
                .end(Json.encodePrettily(products.values()));
    }

    private void createSomeData() {
    	Grammar regle1 = new Grammar("PleinFauteOrtho", "Plein de fautes");
        products.put(regle1.getId(), regle1);
        Grammar regle2 = new Grammar("L'entreprise a procédé au désamiantage suite a la demande du maître d'ouvrage. ",
        		"Homophones a/à",	"avait ou non ?",
        		"L'entreprise a procédé au désamiantage suite à la demande du maître d'ouvrage. ",
        		"Penser à essayer 'avait' dès qu'on s'apprête à écrire 'a' tout seul ! Si 'avait' peut remplacer a dans la phrase , pas d'accent. Sinon : un accent");
        products.put(regle2.getId(), regle2);
    }

}
