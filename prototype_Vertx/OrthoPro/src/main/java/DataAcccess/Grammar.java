package DataAcccess;

import java.util.concurrent.atomic.AtomicInteger;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="GRAMMAR")
public class Grammar {
	@Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name="ID")
	
	private static final AtomicInteger COUNTER = new AtomicInteger();
	
    private Integer id;
    private String sentence;
    private String difficultytype;
    private String tip;
    private String correction;
    private String advice;

    public Grammar(String sentence, String correction){
    	this.id = COUNTER.getAndIncrement();
    	this.sentence = sentence;
    	this.correction = correction;
    }
    
    public Grammar(String sentence, String difficultytype, String correction, String tip, String advice){
    	this.id = COUNTER.getAndIncrement();
    	this.sentence = sentence;
    	this.correction = correction;
    	this.difficultytype = difficultytype;
    	this.advice = advice;
    	this.tip = tip;
    }
    
    public Integer getId() {
        return id;
    }

    public String getSentence() {
        return sentence;
    }

    public void setSentence(final String pSentence) {
    	sentence = pSentence;
    }

    public String getDifficultytype() {
        return difficultytype;
    }
    
    public String setDifficultytype(final String pDifficultytype) {
        return difficultytype = pDifficultytype;
    }


	public String getTip() {
		return tip;
	}

	public void setTip(String pTip) {
		this.tip = pTip;
	}

	public String getCorrection() {
		return correction;
	}

	public void setCorrection(String pCorrection) {
		this.correction = pCorrection;
	}

	public String getAdvice() {
		return advice;
	}

	public void setAdvice(String advice) {
		this.advice = advice;
	}

    
}
